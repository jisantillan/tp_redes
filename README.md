Analisis de redes con NS-3

Para correr 
1)Dejar los scripts en la ruta  ~/ns3/ns-allinone-3.31/ns-3.31/scratch
2)Luego ir a la ruta ~/ns3/ns-allinone-3.31/ns-3.31 
3)Ejecutar el comando $ ./waf --run tpRedes

Para ver las animaciones
1)Generar el xml corriendo el codigo anterior
2)Luego ir a la ruta ~/ns3/ns-allinone-3.31/ns-3.31/netanim-3.108
3)Abrir la aplicacion netanim con el comando $./NetAnim
4)Desde NetAnim abrir el archivo dumbbell.xml
