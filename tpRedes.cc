#include <string>
#include <fstream>
#include "ns3/core-module.h"
#include "ns3/point-to-point-module.h"
#include "ns3/internet-module.h"
#include "ns3/applications-module.h"
#include "ns3/network-module.h"
#include "ns3/packet-sink.h"
#include "ns3/netanim-module.h"
#include "ns3/point-to-point-layout-module.h"
#include "ns3/flow-monitor-module.h"
// 1.Diseñar un escenario con 3 emisores on/off application, 3
// receptores y dos nodos intermedios. O sea se conectarán los
// 3 emisores a un nodo, luego éste a otro y finalmente éste a los
// 3 destinos finales. Esto es normalmente llamado Dumbbell
// topology. En el sistema deberá tener uno de los emisores
// UDP y los otros 2 TCP. Usar siempre conexiones cableadas.

//      e0 -                           - r0
//              |                         |
//      e1 ------- n0 ------ n1 -------- r1
//              |                         |
//      e2 -                           - r2
//
//  emisores = {e0, e1, e2}
//  receptores = {r0, r1, r2}
//  nodosIntermedios = {n0, n1}  


using namespace ns3;

NS_LOG_COMPONENT_DEFINE ("dumbbell-topology");

int main (int argc, char *argv[])
{
// Utilizamos PointToPointDumbbellHelper para crear la topologia usando links punto a punto

// ns3::PointToPointDumbbellHelper::PointToPointDumbbellHelper 	( 	uint32_t  	nLeftLeaf, 
// 		                                                              PointToPointHelper  	leftHelper,
// 		                                                              uint32_t  	nRightLeaf,
//                                                               		PointToPointHelper  	rightHelper,
// 		                                                              PointToPointHelper  	bottleneckHelper 
// 	                                                             ) 	

// Creacion de nodos de acuerdo a dumbbell topology 
  NodeContainer emisores,receptores,nodos;

  emisores.Create(3);
  receptores.Create(3);
  nodos.Create(2);                 

//Construimos links punto a punto que luego usaremos en PointToPointDumbbellHelper

PointToPointHelper p2pRamaIzq; //Se utiliza para linkear los nodos de la rama izquierda y el nodo(router) intermedio más a la izquierda.
p2pRamaIzq.SetDeviceAttribute    ("DataRate", StringValue ("10Mbps"));
p2pRamaIzq.SetChannelAttribute   ("Delay", StringValue ("1ms"));

PointToPointHelper p2pRamaDer;//Se utiliza para linkear los nodos de la rama derecha y el nodo(router) intermedio más a la derecha.
p2pRamaDer.SetDeviceAttribute    ("DataRate", StringValue ("10Mbps"));
p2pRamaDer.SetChannelAttribute   ("Delay", StringValue ("1ms"));

PointToPointHelper p2pNodosIntermedios;// Se utiliza para linkear los routers (bottleneck)
p2pNodosIntermedios.SetDeviceAttribute    ("DataRate", StringValue ("10Mbps"));
p2pNodosIntermedios.SetChannelAttribute   ("Delay", StringValue ("1ms"));

//Creamos una instancia de PointToPointDumbbellHelper
uint32_t cantNodosRamaIzq = 3;  // nLeftLeaf
uint32_t cantNodosRamaDer = 3; //nRightLeaf
PointToPointDumbbellHelper dumbbell (cantNodosRamaIzq, p2pRamaIzq ,cantNodosRamaDer, p2pRamaDer, p2pNodosIntermedios);

// AnimationInterface::SetConstantPosition 	( 	Ptr< Node >  	n,
//		double  	x,
//		double  	y,       ---> con esto solucionamos problema al dibujar?
//		double  	z = 0 
//	) 
//rama izquierda
AnimationInterface::SetConstantPosition(dumbbell.GetLeft(0),1.0,1.0);
AnimationInterface::SetConstantPosition(dumbbell.GetLeft(1),1.0,5.0);
AnimationInterface::SetConstantPosition(dumbbell.GetLeft(2),1.0,10.0);
// //intermedios
AnimationInterface::SetConstantPosition(dumbbell.GetLeft(),5.0,5.0);
AnimationInterface::SetConstantPosition(dumbbell.GetRight(),7.0,5.0);

// //rama derecha
AnimationInterface::SetConstantPosition(dumbbell.GetRight(0),10.0,1.0);
AnimationInterface::SetConstantPosition(dumbbell.GetRight(1),10.0,5.0);
AnimationInterface::SetConstantPosition(dumbbell.GetRight(2),10.0,10.0);




//Instalamos Internet stack (TCP, UDP, IP, etc.) en cada uno de los nodos de dumbbell.

 InternetStackHelper stack;
 dumbbell.InstallStack (stack);

 //Asignamos direcciones IP desde una red, usando la máscara 255.255.255.0 para definir los bits asignables
dumbbell.AssignIpv4Addresses (Ipv4AddressHelper ("10.1.1.0", "255.255.255.0"),
                              Ipv4AddressHelper ("10.2.1.0", "255.255.255.0"),
                               Ipv4AddressHelper ("10.3.1.0", "255.255.255.0"));



/*

  1) Aca hace lo de enviar los paquetes, manda tanto udp como tcp
  
  2)Para el punto 2 habria que sacar los paquetes udp, y buscar la forma de mostrar todo lo que 
  pide
   
  3)Idem punto 3, pero sacando los tcp



*/
//Instanciamos 2 objetos OnOffHelper udp y tcp, para luego trabajar con las aplicaciones on/off 
  OnOffHelper tcpOF ("ns3::TcpSocketFactory",Address());
  OnOffHelper udpOF ("ns3::UdpSocketFactory",Address());
// Seteamos las aplicaciones
    tcpOF.SetAttribute ("OnTime", StringValue ("ns3::ConstantRandomVariable[Constant=1]"));
    tcpOF.SetAttribute ("OffTime", StringValue ("ns3::ConstantRandomVariable[Constant=0]"));

    udpOF.SetAttribute ("OnTime", StringValue ("ns3::ConstantRandomVariable[Constant=1]"));
    udpOF.SetAttribute ("OffTime", StringValue ("ns3::ConstantRandomVariable[Constant=0]"));
  
  uint16_t tcpPort = 50000;
  uint16_t udpPort = 50001;


  PacketSinkHelper tcpSink ("ns3::TcpSocketFactory",InetSocketAddress (Ipv4Address::GetAny (), tcpPort));
  PacketSinkHelper udpSink ("ns3::UdpSocketFactory", InetSocketAddress (Ipv4Address::GetAny (), udpPort));

  ApplicationContainer appsDeReceptores;//holds a vector of ns3::Application pointers. 
  ApplicationContainer appsDeEmisores;//The helper Install method takes a NodeContainer which holds some number of Ptr<Node>. For each of the Nodes in the NodeContainer the helper will
                                    // instantiate an application, install it in a node and add a Ptr<Application> to that application
                                   // into a Container for use by the caller. This is that container used to hold the Ptr<Application> which are instantiated by the Application helper. 


  // Seteamos aplicacion onOff a e0 para enviar a r0 bajo TCP 
  // appsDeEmisores.Add (setUpApplication (tcpOF, dumbbell.GetLeft (0), dumbbell.GetRightIpv4Address (0), tcpPort)); hacer metodo que haga esto para no repetir tanto, ahora lo hago hardcodeado
  AddressValue address (InetSocketAddress (dumbbell.GetRightIpv4Address (0), tcpPort));
  tcpOF.SetAttribute ("Remote", address);
  appsDeEmisores.Add (tcpOF.Install(dumbbell.GetLeft (0)));
  appsDeReceptores.Add (tcpSink.Install (dumbbell.GetRight (0)));
  // Seteamos aplicacion onOff a e1 para enviar a r1 bajo TCP 
 // appsDeEmisores.Add (setUpApplication (tcpOnOF, dumbbell.GetLeft (1), dumbbell.GetRightIpv4Address (1), tcpPort));
  AddressValue address2 (InetSocketAddress (dumbbell.GetRightIpv4Address (1), tcpPort));
  tcpOF.SetAttribute ("Remote", address2);
  appsDeEmisores.Add (tcpOF.Install(dumbbell.GetLeft (1)));
  appsDeReceptores.Add (tcpSink.Install (dumbbell.GetRight (1)));
  // Seteamos aplicacion onOff a e2 para enviar a r2 bajo UDP 
  //appsDeEmisores.Add (setUpApplication (udpOF, dumbbell.GetLeft (2), dumbbell.GetRightIpv4Address (2), udpPort));
  AddressValue address3 (InetSocketAddress (dumbbell.GetRightIpv4Address (2), udpPort));
  tcpOF.SetAttribute ("Remote", address3);
  appsDeEmisores.Add (tcpOF.Install(dumbbell.GetLeft (2)));
  appsDeReceptores.Add (tcpSink.Install (dumbbell.GetRight (2)));
      


//Iniciamos las aplicaciones
 appsDeEmisores.Start (Seconds (1.0));
 appsDeEmisores.Stop (Seconds (20.0));
 appsDeReceptores.Start (Seconds (0.0));
 appsDeReceptores.Stop (Seconds (20.0));

 // Creamos objeto AnimationInteface y seteamos que queremos que muestre
      AnimationInterface anim ("dumbbell.xml");
      anim.EnableIpv4L3ProtocolCounters (Seconds (0), Seconds (20));
      
     // Seteamos la simulacion 
     Ipv4GlobalRoutingHelper::PopulateRoutingTables ();
   
     // Flow monitor
    // Ptr<FlowMonitor> flowMonitor;
     //FlowMonitorHelper floAnimationInterface::SetConstantPosition(dumbbell.GetLeft(0),1.0,1.0);
// AnimationInterface::SetConstantPosition(dumbbell.GetLeft(1),1.0,5.0);
// AnimationInterface::SetConstantPosition(dumbbell.GetLeft(2),1.0,10.0);
// //intermedios
// AnimationInterface::SetConstantPosition(dumbbell.GetLeft(),5.0,5.0);
// AnimationInterface::SetConstantPosition(dumbbell.GetRight(),7.0,5.0);

// //rama derecha
// AnimationInterface::SetConstantPosition(dumbbell.GetRight(0),10.0,1.0);
// AnimationInterface::SetConstantPosition(dumbbell.GetRight(1),10.0,5.0);
// AnimationInterface::SetConstantPosition(dumbbell.GetRight(2),10.0,10.0);wHelper;
    // flowMonitor = flowHelper.InstallAll(); 
//     anim.SetConstantPosition(dumbbell.GetLeft(0),1.0,1.0);
//     anim.SetConstantPosition(dumbbell.GetLeft(1),1.0,10.0);
// anim.SetConstantPosition(dumbbell.GetLeft(2),1.0,20.0);
// //intermedios
// anim.SetConstantPosition(dumbbell.GetLeft(),10.0,10.0);
// anim.SetConstantPosition(dumbbell.GetRight(),20.0,10.0);

// //rama derecha
// anim.SetConstantPosition(dumbbell.GetRight(0),30.0,1.0);
// anim.SetConstantPosition(dumbbell.GetRight(1),30.0,10.0);
// anim.SetConstantPosition(dumbbell.GetRight(2),30.0,20.0);
  //    anim.EnablePacketMetadata (true); 
     Simulator::Stop(Seconds(20.0));
     Simulator::Run ();
     Simulator::Destroy ();

   //  flowMonitor->SerializeToXmlFile("dumbbell.xml", true, true);  
   return 0; 
 }


